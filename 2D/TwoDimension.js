const Geometry = require("../Geometry");

class TwoDimension extends Geometry {
  constructor(name) {
    super(name, "Two Dimention");
  }

  //   whoAmI() {
  //     console.log(`Im ${this.type}`);
  //   }

  calculateCircumference() {
    return console.log("===== Calculating Circumference =====");
  }

  calculateArea() {
    return console.log("========= Calculating Area ==========");
  }
}

module.exports = TwoDimension;
