const ThreeDimention = require("./ThreeDimension");

class Cube extends ThreeDimention {
  constructor(length) {
    super("Cube");
    this.length = length;
    Cube.count++;
  }

  // Overloading
  calculateVolume(name) {
    super.calculateVolume();
    const result = this.length ** 3;
    console.log(`${name} is trying to calculate Volume: ${result} cm`);
    return result;
  }

  calculateArea(name) {
    super.calculateArea();
    const result = 6 * this.length ** 2;
    console.log(`${name} is trying to calculate Area: ${result} cm2`);
    return result;
  }
}

module.exports = Cube;
