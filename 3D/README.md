
| Nama bangun ruang | Luas | Volume |
| ------ | ------ | ------ |
| Kubus | L = 6 x (s x s) | V =  s x s x s |
| Balok | L = 2 x (pl + lt + pt) | V = p x l x t |
| Tabung | L = (2 x La) + (Ka x tinggi) | V = luas alas x t |
| Kerucut | L = 4 x π x r² | V = 4/3 x π x r³ |
