const ThreeDimension = require("./ThreeDimension");

class Tube extends ThreeDimension {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
  }

  calculateArea(name) {
    // 2 πr (r+t)
    super.calculateArea();
    const formula = 2 * Math.PI * this.radius * (this.radius + this.height);
    return `${name} : ${formula}`;
  }

  calculateVolume(name) {
    // V = π x r^2 x t
    super.calculateVolume();
    const formula = Math.PI * this.radius * this.radius * this.height;
    return `${name} : ${formula}`;
  }
}

module.exports = Tube;
